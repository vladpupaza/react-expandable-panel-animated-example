import React from 'react';
import PropTypes from 'prop-types';

const CompD = (props) => (
    <div>
        <div>Some Info</div>
        <div>Some Info</div>
        <div>Some Info</div>
        <div>Some Info</div>
        <div>Some Info</div>
        <div>Some Info</div>
        <div>Some Info</div>
        <div>Some Info</div>
    </div>
);

CompD.propTypes = {
    onExpand: PropTypes.func
};

export default CompD;