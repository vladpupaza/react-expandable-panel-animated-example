import React from 'react';
import CompD from './CompD';
import styled from 'styled-components';

const Description = styled.div`
    display: flex;
`;

const CompC = (props) => (
    <div>
        <div>
            {
                [1,2,3,4].map((child, index) =>
                    <div key={`CompC_${index}`}>
                        <h2>SubTitle</h2>
                        <img
                            src="http://useravatars.cgdata.com/245144_cgeu-8k-Y8-avatar2733655-1.gif.jpg"
                            alt={`img100x100_${child}`} />
                        <Description>
                            <div>Item Description</div>
                            <CompD onExpand={props.onExpand} />
                        </Description>
                    </div>
                )
            }
        </div>
    </div>
);

CompC.propTypes = {
};

export default CompC;