import React from 'react';
import CompC from './CompC';
import PropTypes from 'prop-types';
import styled, { keyframes } from 'styled-components';

const expand = keyframes`
    from {
        height: 0;
    }
    to {
        height: 600px;
    }
`;

const collapse = keyframes`
    from {
        height: 600px;
    }
    to {
        height: 0px;
    }
`;

const SlidingDiv = styled.div`
    height: 600px;
    overflow: auto;
    position: relative;
    animation: 1s ${(props) => props.expanded ? expand : collapse} forwards;
`;

const PanelContent = (props) => (
    <SlidingDiv expanded={props.expanded}>
        <h1>Title</h1>
        <img
            src="https://goo.gl/L7yie4"
            alt="img300x300" />
        <CompC />
    </SlidingDiv>
);

PanelContent.propTypes = {
    onExpand: PropTypes.func
};

export default PanelContent;