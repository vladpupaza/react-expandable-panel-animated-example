import React from 'react';
import PropTypes from 'prop-types'
import PanelContent from './PanelContent';

const ExpandablePanel = (props) => {
    const onExpand = () => props.onExpand(props.componentName);
    return (
        <div className="expandablePanal">
            <div>
                Expandable Component
                <button onClick={onExpand}>Click to expand</button>
            </div>
            {
                props.expanded !== undefined &&
                <PanelContent
                    expanded={props.expanded}
                    onExpand={props.onExpand} />
            }
        </div>
    );
};

ExpandablePanel.propTypes = {
    expanded: PropTypes.bool,
    onExpand: PropTypes.func,
    componentName: PropTypes.string
};

export default ExpandablePanel;