import React, { Component } from 'react';
import ExpandablePanel from './ExpandablePanel';

const listOfComponentNames = [
    'ExpandablePanel1',
    'ExpandablePanel2',
    'ExpandablePanel3',
    'ExpandablePanel4'
];

class CompA extends Component {
    constructor() {
        super();
        this.onExpand = this.onExpand.bind(this);
        this.expandCollapseAll = this.expandCollapseAll.bind(this);
        this.state = {
            expanded: false
        }
    }
    expandCollapseAll(){
        const newState = Object.keys(this.state).reduce((acc, key) => {
            acc[key] = false;
            return acc;
        }, {});
        this.setState(newState);
    }
    onExpand(componentName) {
        this.setState({
            [componentName]: !this.state[componentName]
        })
    }
    render() {
        return (
            <div>
                <button onClick={this.expandCollapseAll}>Collaptse All</button>
                <div className="container">
                    {
                        listOfComponentNames.map(name =>
                            <ExpandablePanel
                                key={name}
                                componentName={name}
                                expanded={this.state[name]}
                                onExpand={this.onExpand}/>
                        )
                    }
                </div>
            </div>
        );
    }
}

export default CompA;